# PROGRAMMATION OBJECT


<details>
  <summary>SET UP</summary>

## If you use the Dev Container

If you install [ms-vscode-remote.remote-containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) and [Docker](https://www.docker.com/products/docker-desktop), you will be able to run your code inside a Linux container (kind of like a virtual machine, but faster). Also, you will get static analyzers, code formatters and useful extensions installed out of the box! It is a great option to get started with C++ quickly.

(Unfortunately, if you want to do GUI applications they don't work well from within a container and you might have to do a proper setup on your own desktop instead. But for simple command-line applications this works amazingly well!)

NB: the container might take a while to build the first time.

## If you don't use the Dev Container

### Compiling

You need to install [CMake](https://cmake.org/download/).

To use CMake I recommend this VS Code extension : [ms-vscode.cmake-tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools). You will need to setup the extension with a compiler. Here is [the tutorial](https://code.visualstudio.com/docs/cpp/cmake-linux). It is based on Linux but at the bottom of the page you will find the explanations to adapt it for [Windows](https://code.visualstudio.com/docs/cpp/config-msvc) and [Mac](https://code.visualstudio.com/docs/cpp/config-clang-mac).

Alternatively you can just create a *build* folder at the root of this project, open a terminal and run `cmake ..`; chances are it will detect what compiler you have installed and generate the appropriate Makefile / Visual Studio solution / Xcode project.

### Auto-formatting

[Check this out](https://julesfouchy.github.io//Learn--Clean-Code-With-Cpp/lessons/formatting-tool) to learn why you would want to use a code formatter and how to do it.

### Static analysis

[Check this out](https://julesfouchy.github.io/Learn--Clean-Code-With-Cpp/lessons/static-analysis-and-sanitizers) to learn why you would want to use static analysis and how to do it.

</details>

🔴​🟠​🟡​🟢​🔵​🟣​⚫️​⚪️​🟤​🔴​🟠​🟡​🟢​🔵​🟣​⚫️​⚪️​🟤​🔴​🟠​🟡​🟢​🔵​🟣​⚫️​⚪️​🟤​🔴​🟠​🟡​🟢​🔵​🟣​⚫️​⚪️​🟤​🔴​🟠​🟡​🟢​🔵​🟣​⚫️​⚪️​🟤🔴​🟠​🟡​🟢​🔵​🟣​⚫️​

Meaning : 
  - 🟠 - read but not really understood
  - ​🟡​ - globally I get the concept
  - 🟢​ - understood
  - 🔵​ - didn't read completely or chapter not finished

## CHAPITRES LUS (click on the levels to see more 😉)

<details>
  <summary>LEVEL 1</summary>
  ​
  
- [x] 🟢​ Install a compiler
- [x] 🟢​ Use an IDE
- [x] 🟢​ Use Git
- [x] 🟢​ Use a debugger
- [x] 🟢​ Use a formatting tool
- [x] 🟢​ Use static analysers
- [x] 🟢​ Naming
- [x] 🟢​ Stack vs Heap
- [x] 🟢​ Consistency in style

</details>

<details>
  <summary>LEVEL 2</summary>
  
- [x] 🟢 Make it work, then make it good
- [x] 🟢 Prefer free functions  
  finally understand what it is 
  any function not part of a class
- [x] 🟡 Design cohesive classes  
  Not sure I understand what invariants are
  Prefer using a struct over a class if you have no invariants to enforce 
- [x] 🟡 Use structs to group data  
  See designated initializers
- [x] 🟢 Write small functions
- [x] DRY: Don't repeat yourself  
  Code duplication doesn't mean textual duplication, but knowledge duplication
  SUMMARY:
  Use a **loop** or an algorithm, when you want to do the same thing multiple times in a row.  
  Use a **function**, when you want to do the same thing in different places.  
  Use a **template**, when you want to do the same thing but for different types.  
  Use a **struct or a class**, when you want to use the same group of data in different places.  
  Use **auto**, when the type is already declared in the expression.  
- [x] 🟡 Enums
- [x] 🟢 Split problems in small pieces
- [x] 🟡 Composition over Inheritance

</details>

<details>
  <summary>LEVEL 3</summary>
  
- [x] 🟢 std::vector	
- [x] 🟢 Documentation
- [x] 🟢 Use libraries
- [x] 🟡 assert  
  Makes sure that a function is used properly
  Asserts are removed in release mode and exceptions remain
  static_assert happens at compile time, whereas assert happens at runtime
- [x] 🟢 auto	
- [X] 🔵 Testing	
- [ ] 🔵 Type design ??? doesn't exist anymore ?
- [X] 🟢 Minimize Dependencies	
- [ ] 🔵 Lambda functions
- [X] 🟢 Master your IDE	
- [X] 🟠 std::optional	
- [X] 🟢 Single Source of Truth
  Things are stored only once
- [X] 🟢 Strong Typing
  type used in place of another type to carry specific meaning through its name
  mieux exprimer le nom des variables pour ne pas se tromper avec d'autres
- [X] 🔵 Code reviews
- [X] 🟢 const	
- [X] 🟢 Don't overfocus on performance
- [X] 🟢 Git submodules
  C'était une galère mais we made it with p6
- [X] 🟡 STL algorithms
  à implémenter pour voir
- [ ] 🔵 Debug vs Release
- [ ] 🔵 Immediately Invoked Function
- [x] 🟢 Markdown
- [X] 🟢 Range-based for loop
  Utiliser const auto et ses différentes configs pour limiter les erreurs de boucle for
- [X] 🟡 Smart Pointers
  Arf les pointeurs, ça a toujours été mon point faible...
- [X] 🟠 std::function
  ummm je suis pas sûre
  quelle est la différence avec juste appeler une fonction dans une autre ?
- [X] 🟠 Dependency Injection
  un peu perdue parce que dans le cours des dépendances on doit les limiter au max alors que là "adding more parameters to your functions is always the right solution to your problems"
- [X] 🟡 Error Handling
- [X] 🟢 Git Pull Requests
  = merge request : pour proposer des changements/corrections sur un git qui nous appartient pas
- [X] 🟢 Advanced Git features
- [X] 🟡 CMake
  cmake, mon pire ennemi mais je comprend l'interêt
- [X] 🟡 Functional programming
  way of thinking about software construction by creating pure functions (pure function is one whose results are dependent only upon the input parameters)
- [X] 🟢 Move semantics
  if we're not using an object anymore, we can simply copy the pointer to use as another object and save a lot of time = move
  automatique la plupart du temps
- [X] 🟢 The Rule of 5
  if you write a special member function (a.k.a. a destructor, a copy constructor, a copy assignment, a move constructor or a move assignment), then you need to write all 5 of them
  in general, classes shouldn't have explicit special member functions
- [X] 🟡 State and Strategy
  on parle de design pattern ?
- [X] 🟢 std::variant
  variable that can hold either an int or a float : actual type changes at runtime
- [X] 🔵 Cache and Branches
- [ ] 🔵 Multithreading
- [X] 🟡 Polymorphism
- [X] 🟢 Space out your code

</details>


<details>
  <summary>LEVEL 4</summary>
  
- [X] 🟢 Watch conferences
- [X] 🟢 Write libraries
- [X] 🟡 The Command pattern
- [X] 🟢 Dear ImGui
- [ ] 🔵 Designated Initializers
- [X] 🟢 std::string and std::string_view
  Problem with compiling string_view the last time I tried 
- [ ] 🔵 Static site generators
- [X] 🟢 Deleted functions
- [X] 🔵 Type erasure
- [ ] 🔵 Wasm and electron
- [ ] 🔵 Scope guard
- [X] 🟡 C++ casts
- [ ] 🔵 Measuring performance
- [X] 🔵 Precompiled Header
- [ ] 🔵 < random >
- [X] 🟢 Linked lists
- [X] 🟢 Avoid dead code
- [X] 🟢 friend 
- [X] 🔵 Pointers vs references

</details>


<details>
  <summary>LEVEL 5</summary>
  
- [X] 🔵 Avoid nested namespaces 
- [X] 🟢 #if defined()
- [ ] 🔵 Trailing return type

</details>


<details>
  <summary>GAMES & ASSIGNMENT</summary>
  
- [x] 🟢 Guess the number
- [x] 🟢 Hangman  
- [X] 🟢 Adding a menu
- [ ] 🟡 Noughts and Crosses
  (Game not finished - pretty hard to understand how to do it and problems I could not resolve 😢😢😢)
- [ ] Connect 4
- [ ] Adding tests
- [ ] An AI for Hangman
- [ ] Going further

</details>



