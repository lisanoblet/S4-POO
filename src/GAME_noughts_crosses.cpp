#include "../include/GAME_noughts_crosses.hpp"



void draw_board(int size, p6::Context& ctx)
{
    //ctx.background({.3f, 0.25f, 0.35f});
    ctx.background({0.960f, 0.839f, 0.839f});
    ctx.stroke_weight = 0.01f;
    ctx.stroke        = {0.f, 0.f, 0.f, 0.f};
    ctx.fill          = {1.f, 1.f, 1.f, 1.f};

    // maybe change loop with auto
    for (int x = 0; x < size; ++x) {
        for (int y = 0; y < size; ++y) {

            draw_cell({x, y}, size, ctx);
        }
    }
}

void draw_cell(CellIndex index, int board_size, p6::Context& ctx)
{
    ctx.square(p6::BottomLeftCorner{cell_bottom_left_corner(index, board_size)}, p6::Radius{cell_radius(board_size)});
}


float cell_radius(int board_size)
{
    return 1.f / static_cast<float>(board_size);
}

// j'ai vraiment du mal à comprendre cette fonction et celle pour traduire les index avec la position de la souris mais bon
glm::vec2 cell_bottom_left_corner(CellIndex index, int board_size)
{
    const auto idx = glm::vec2{static_cast<float>(index.x), static_cast<float>(index.y)};

    return p6::map(idx, glm::vec2{0.f}, glm::vec2{static_cast<float>(board_size)}, glm::vec2{-1.f}, glm::vec2{1.f});
}


std::optional<CellIndex> cell_hovered_by(glm::vec2 position, int board_size)
{
    const auto pos = p6::map(position, glm::vec2{-1.f}, glm::vec2{1.f}, glm::vec2{0.f}, glm::vec2{static_cast<float>(board_size)});

    const auto index = CellIndex{static_cast<int>(std::floor(pos.x)), static_cast<int>(std::floor(pos.y))};

    if (index.x >= 0 && index.x < board_size && index.y >= 0 && index.y < board_size) {
        return std::make_optional(index);
    }
    else {
        return std::nullopt;
    }
}


glm::vec2 cell_center(CellIndex index, int board_size)
{
    return cell_bottom_left_corner(index, board_size) + cell_radius(board_size);
}



// Couleurs différentes pour noughts et cross
void draw_cross(CellIndex index, int board_size, p6::Context& ctx)
{
    ctx.stroke        = {0, 1., 1.};
    ctx.fill          = {0.f, 1.f, 1.f, 1.f};
    ctx.stroke_weight = 0.4f * cell_radius(board_size);

    ctx.square(p6::Center{cell_center(index, board_size)},
               p6::Radius{0.3f},
               p6::Rotation{0.0_turn});
}

void draw_noughts(CellIndex index, int board_size, p6::Context& ctx)
{
    ctx.stroke        = {1., 0., 1.};
    ctx.fill          = {1.f, 0.f, 1.f, 1.f};
    ctx.stroke_weight = 0.4f * cell_radius(board_size);

    ctx.square(p6::Center{cell_center(index, board_size)},
               p6::Radius{0.3f},
               p6::Rotation{0.0_turn});
}

// la fonction marche pas du tout et je comprend pas pourquoi
// il connait pas board size et je sais pas quoi faire pour contourner
/*
void draw_noughts_and_crosses(const Board<board_size>& board, p6::Context& ctx)
{
    for (int x = 0; x < size; ++x) {
        for (int y = 0; y < size; ++y) {
        }
    }
}


void switch_player(Player& current_player)
{
    if (current_player == Player::Noughts) {
        current_player = Player::Crosses;
    }
    else {
        current_player = Player::Noughts;
    }
}



void place_symbol_on_click(CellIndex index, Board<board_size>& board, Player& current_player)
{
    const bool cell_is_empty = !board[*index].has_value();

    if (cell_is_empty) {
        board[*index] = current_player;
        switch_player(current_player);
    }
}
*/


void play_noughts_crosses()
{


    static constexpr int board_size = 3;


    Board<board_size> board;
    auto              current_player = Player::Crosses;
    auto              ctx            = p6::Context{{800, 800, "Noughts and Crosses"}};

    /*

    ctx.mouse_pressed = [&](p6::MouseButton event) {
        place_symbol_on_click(cell_hovered_by(event.position, board_size), board, current_player);
    };
*/

    ctx.update = [&]() {
        draw_board(board_size, ctx);

        auto hovered_cell = cell_hovered_by(ctx.mouse(), board_size);
        if (hovered_cell.has_value()) {

            draw_cross(*hovered_cell, board_size, ctx);
            draw_noughts(*hovered_cell, board_size, ctx);
        }
        //draw_noughts_and_crosses(board, ctx);
        //draw_noughts_and_crosses<board_size>(board, ctx);
        //draw_noughts_and_crosses(board<board_size>, ctx);
    };





    ctx.start();
}