#include "../include/get_input.hpp"
#include <iostream>
#include <random>
#include "../include/random.hpp"

template<typename T>
T get_input_from_user()
{
    T input_given;
    std::cout << "Enter an input to play the game :" << std::endl;
    std::cin >> input_given;
    return input_given;
}
