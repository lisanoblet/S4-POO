#include "../include/GAME_guess_number.hpp"
#include <iostream>
#include <random>
#include "../include/get_input.hpp"
#include "../include/random.hpp"

void play_guess_the_number()
{
    static constexpr int MIN             = 0;   // `static constexpr` is the "proper" way of declaring constants known at compile time
    static constexpr int MAX             = 100; // It is as efficient as `#define` but has the benefit of working like a normal C++ variable: it has a type, etc.
    const int            number_to_guess = rand(MIN, MAX);

    bool finished = false;

    while (!finished) {
        int number_player = get_input_from_user<int>();

        if (number_player < number_to_guess) {
            std::cout << "Nombre trop petit" << std::endl;
        }
        else if (number_player > number_to_guess) {
            std::cout << "Nombre trop grand" << std::endl;
        }
        else {
            std::cout << "Bravoooo" << std::endl;
            finished = true;
        }
    }
}
