#include "../include/GAME_hangman.hpp"
#include "../include/get_input.hpp"
#include "../include/random.hpp"



// picks a word to play in the list
std::string pick_a_random_word()
{
    static constexpr std::array words_list = {
        "vivement",
        "le",
        "weekend",
        "pfiou",
        "youpi",
        "incroyable",
        "ce",
        "jeu",
        "marche",
        "je",
        "suis",
        "trop",
        "contente",

    };

    //choix du mot au hasard
    //int list_length = words_list.size() - 1;
    //n'acceptait pas que je mette directement words_list.size() - 1 dans le choix du random ¯\_(ツ)_/¯

    //int random_number = rand(0, list_length);

    //std::string chosen_word = words_list[random_number];
    std::string chosen_word = words_list[rand(0, words_list.size() - 1)];

    return chosen_word;
}


//PARTIE SUR LE JEU HANGMAN

// shows the word with the letters found and dashes when not found
void show_word_to_guess_with_missing_letters(const std::string& word, const std::vector<bool>& letters_guessed)
{

    assert(word.size() == letters_guessed.size()); // Its important to assert to make sure that your assumptions are actually checked in code

    for (long unsigned int i = 0; i < word.size(); i++) {
        if (letters_guessed[i] == true) {
            std::cout << word[i];
        }
        else {
            std::cout << "_";
        }
    }
    std::cout << " " << std::endl;
    std::cout << " " << std::endl;
}

// return boolean whether letter found in chosen word
bool word_to_guess_contains(char letter, const std::string& word)
{
    // ne connait apparemment pas de contains dans std
    //return std::string_view(word).contains(letter);
    return word.find(letter) != std::string::npos;
}

//fonction copiée, j'y arrivais pas
//transform dash into letter guessed
void mark_as_guessed(char guessed_letter, std::vector<bool>& letters_guessed, std::string word_to_guess)
{
    assert(word_to_guess.size() == letters_guessed.size()); // Its important to assert to make sure that your assumptions are actually checked in code
    std::transform(letters_guessed.begin(), letters_guessed.end(), word_to_guess.begin(), letters_guessed.begin(), [&](bool b, char letter) {
        if (guessed_letter == letter) {
            return true;
        }
        else {
            return b;
        }
    });
}

//returns boolean if all letters have been found in the word
bool player_has_won(const std::vector<bool>& letters_guessed)
{
    // exemples avec les algos de la librairie standard
    // all true
    //std::all_of(vec.begin(), vec.end(), [](bool v) { return v; });
    // all false
    //std::all_of(vec.begin(), vec.end(), [](bool v) { return !v; });
    //std::none_of(vec.begin(), vec.end(), [](bool v) { return v; });

    return std::all_of(letters_guessed.begin(), letters_guessed.end(), [](bool letter_guessed) {
        return letter_guessed == true;
    });
}

// PARTIES SUR LES VIES

// DISPLAY FUNCTIONS

bool player_is_alive(int number_of_lives)
{
    return number_of_lives != 0;
    //pour éviter l'erreur de redondance avec deux return true/false
}

void remove_one_life(int& number_of_lives)
{
    number_of_lives--;
}

void show_number_of_lives(int number_of_lives)
{
    if (number_of_lives > 5) {
        std::cout << "Number of lives remaining : " << number_of_lives << std::endl;
    }
    else if (number_of_lives >= 2) {
        std::cout << "Time to be careful, you don't have many chances left" << std::endl;
    }
    else {
        std::cout << "Mmmmhhhh good luck with your last chance" << std::endl;
    }
    //std::cout << "You have " << number_of_lives << " lives remaining." << std::endl;
}


// PARTIE SUR LA VICTOIRE DEFAITE

void show_congrats_message()
{
    std::cout << "Well done ! You won the game !" << std::endl;
}

void show_defeat_message()
{
    std::cout << "bruh you're such a bad player :( better luck next time " << std::endl;
}


// HANGMAN GAME LOOP
void play_hangman()
{
    int               number_of_lives = 10;
    const std::string word            = pick_a_random_word();
    std::vector<bool> letters_guessed(word.size(), false);

    while (player_is_alive(number_of_lives) && !player_has_won(letters_guessed)) {

        show_word_to_guess_with_missing_letters(word, letters_guessed);
        show_number_of_lives(number_of_lives);

        const auto guess = get_input_from_user<char>();

        if (word_to_guess_contains(guess, word)) {
            mark_as_guessed(guess, letters_guessed, word);
        }
        else {
            remove_one_life(number_of_lives);
        }
    }
    if (player_has_won(letters_guessed)) {
        show_congrats_message();
        std::cout << "The word you correctly guessed was :" << std::endl;
        show_word_to_guess_with_missing_letters(word, letters_guessed);
    }
    else {
        show_defeat_message();
    }
}



/* 
// ANCIENS TESTS
//bool found = false;

    //std::cout << chosen_word << std::endl;

    
    while (player_is_alive() && !player_has_won()) {

    };

    char letter_player = get_input_from_user<char>();

    for (uint i = 0; i < chosen_word.size() - 1; i++) {
        if (letter_player == chosen_word[i]) {
            std::cout <<
        }
    }

    while (!found) {
        if (letter_player < number_to_guess) {
            std::cout << "Nombre trop petit" << std::endl;
        }
    }

    */
