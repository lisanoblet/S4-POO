#include "../include/MENU.hpp"
#include "../include/GAME_guess_number.hpp"
#include "../include/GAME_hangman.hpp"
#include "../include/GAME_noughts_crosses.hpp"
#include "../include/get_input.hpp"


void choose_game()
{

    std::cout << "Choose a game to play !" << std::endl;
    std::cout << "Press 1 to play GUESS A NUMBER" << std::endl;
    std::cout << "Press 2 to play HANGMAN" << std::endl;
    std::cout << "Press 3 to play NOUGHTS & CROSSES" << std::endl;
    std::cout << "Press q to quit" << std::endl;

    bool game_not_chosen = true;

    while (game_not_chosen) {
        const auto game_choice = get_input_from_user<char>();

        switch (game_choice) {
        case '1':
            std::cout << "GREAT ! Let's play GUESS THE NUMBER" << std::endl;
            play_guess_the_number();
            game_not_chosen = false;
            break;
        case '2':
            std::cout << "GREAT ! Let's play HANGMAN" << std::endl;
            play_hangman();
            game_not_chosen = false;
            break;
        case '3':
            std::cout << "GREAT ! Let's play NOUGHTS & CROSSES" << std::endl;
            play_noughts_crosses();
            game_not_chosen = false;
            break;
        case 'q':
            std::cout << "You quit the game !" << std::endl;
            game_not_chosen = false;
            break;
        default:
            std::cout << "You have to choose a valid game" << std::endl;
            game_not_chosen = true;
        }
    }
}
