#pragma once
#include <p6/p6.h>
#include <iostream>


struct CellIndex {
    int x;
    int y;
};


enum class Player {
    Noughts,
    Crosses,
};

template<int size>
class Board {
public:
    std::optional<Player>& operator[](CellIndex index)
    {
        return _cells[index.x][index.y];
    }

    const std::optional<Player>& operator[](CellIndex index) const
    {
        return _cells[index.x][index.y];
    }

private:
    std::array<std::array<std::optional<Player>, size>, size> _cells;
};


template<int size>
void draw_noughts_and_crosses(const Board<size>& board, p6::Context& ctx);


void play_noughts_crosses();


// Drawing

float cell_radius(int board_size);

glm::vec2 cell_bottom_left_corner(CellIndex index, int board_size);

void draw_board(int size, p6::Context& ctx);

void draw_cell(CellIndex index, int board_size, p6::Context& ctx);

void draw_noughts(std::optional<CellIndex> hovered_cell, int board_size, p6::Context& ctx);
void draw_cross(std::optional<CellIndex> hovered_cell, int board_size, p6::Context& ctx);

glm::vec2 cell_center(CellIndex index, int board_size);


// Hovering and position detection
glm::vec2 cell_hovered(p6::Context& ctx);

std::optional<CellIndex> cell_hovered_by(glm::vec2 position, int board_size);


// Game logic

void switch_player(Player& current_player);

// void place_symbol_on_click(std::optional<CellIndex> index, Board<board_size>& board, Player& current_player);