#pragma once

#include <algorithm>
#include <array>
#include <cassert>
#include <iostream>
#include <random>
#include <string>
//#include <string_view>


std::string pick_a_random_word();

void show_word_to_guess_with_missing_letters(const std::string& word, const std::vector<bool>& letters_guessed);

bool word_to_guess_contains(char letter, const std::string& word);

void mark_as_guessed(char guessed_letter, std::vector<bool>& letters_guessed, std::string word_to_guess);

bool player_has_won(const std::vector<bool>& letters_guessed);

bool player_is_alive(int number_of_lives);

void remove_one_life(int& number_of_lives);

void show_number_of_lives(int number_of_lives);

void show_congrats_message();

void show_defeat_message();

void play_hangman();
