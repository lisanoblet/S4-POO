#pragma once
#include <iostream>
#include <random>

template<typename T>
T get_input_from_user()
{
    T input_given;
    std::cout << "Enter an input to play the game :" << std::endl;
    std::cin >> input_given;
    return input_given;
}